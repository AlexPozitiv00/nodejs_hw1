const path = require('path');
const fs = require('fs');
const fsPromises = fs.promises;

const compareExt = async (req, res, next) => {
    const {fileName, content} = req.body;
    const extArr = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
    const fileExt = path.extname(fileName);

    if (!fileName || !fileName.trim()) {
        return res.status(400).json({ message: "Please specify 'filename' parameter" });
    }

    if (!content || !content.trim()) {
        return res.status(400).json({ message: "Please specify 'content' parameter" });
    }   

    if(!extArr.includes(fileExt)) {
        return res.status(400).json({message: 'Not supported file type'});
    }

    if (fs.existsSync(`./files/${fileName}`)) {
        return res.status(400).json({ message: `File already exists` });
    }  

    if (!fs.existsSync('./files')) {
        try {
          await fsPromises.mkdir('./files');
        } catch {
          console.log('Failed to create directory');
        }
    }

    next();
}

module.exports = compareExt;