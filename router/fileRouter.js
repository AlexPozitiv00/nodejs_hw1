const express = require('express');
const router = express.Router();

const compareExt = require('../compare/compareExt');
const {createFile, getFiles, getFileByName, updateFile, removeFile} = require('../controller/fileController');

router.get('/', getFiles);
router.get('/:fileName', getFileByName);

router.post('/', compareExt, createFile);

router.put('/', updateFile);

router.delete('/:fileName', removeFile);

module.exports = router;