const fs = require('fs').promises;
const path = require('path');

const createFile = async (req, res) => {
    const {fileName, content} = req.body;

    try {
        await fs.writeFile(`./files/${fileName}`, content, 'utf-8');
        res.status(200).json({message: 'File created successfully'});
    } catch {
        res.status(500).json({message: 'Server error'});
    }
}

const getFiles = async (req, res) => {
    try {
        const fileArr = await fs.readdir('./files');

        return res.status(200).json({
            message: 'Success',
            files: fileArr
        });
    } catch {
        res.status(500).json({ message: 'Server error' })
    }
      
}

const getFileByName = async (req, res) => {
    const fileName = req.params.fileName;
    const fileExt = path.extname(fileName);
    const fileArr = await fs.readdir('./files');

    if (!fileArr.includes(fileName)) {
        return res.status(400).json({ message: `No file with ${fileName} filename found` });
    }
    
    try {
        const {birthtime} = await fs.stat(`./files/${fileName}`);
        const content = await fs.readFile(`./files/${fileName}`, 'utf-8');

        const result = {
        message: 'Success',
        filename: fileName,
        content: content,
        extension: fileExt,
        uploadedDate: birthtime
       }  

        res.status(200).json(result);
    } catch {
        res.status(500).json({ message: 'Server error' });
    }
  

}

const updateFile = async (req, res) => {
    const fileName = req.body.fileName;
    const content = req.body.content;

    try {
        await fs.writeFile(`./files/${fileName}`, content, 'utf-8');
        res.status(200).json({message: 'File has been updated'});
    } catch {
        res.status(500).json({message: 'Server error'});
    }
}

const removeFile = async (req, res) => {
    const fileName = req.params.fileName;
    console.log(fileName)
    try {
        await fs.unlink(`./files/${fileName}`);
        res.status(200).json({message : 'File has been deleted'});
    } catch {
        res.status(500).json({message : 'Server error'});
    }
}

module.exports = {createFile, getFiles, getFileByName, updateFile, removeFile};