const express = require('express');
const morgan = require('morgan');
const fileRouter = require('./router/fileRouter');

const PORT = 8080;

const app = express();

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/files', fileRouter);

app.listen(PORT, () => {
    console.log(`Server starts on port ${PORT}`);
})